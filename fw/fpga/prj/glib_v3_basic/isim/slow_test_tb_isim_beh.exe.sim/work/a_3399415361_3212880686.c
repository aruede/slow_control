/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x8ef4fb42 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/slow_control/fw/fpga/src/user/usr/slow_control/slow_test.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1258338084_503743352(char *, char *, unsigned int , unsigned int );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3399415361_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int t11;
    int t12;

LAB0:    xsi_set_current_line(38, ng0);
    t1 = (t0 + 568U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3396);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(39, ng0);
    t3 = (t0 + 1236U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 3464);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB6:    xsi_set_current_line(45, ng0);
    t1 = (t0 + 1788U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)3);
    if (t5 != 0)
        goto LAB8;

LAB10:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 1236U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (t0 + 3500);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t2;
    xsi_driver_first_trans_fast(t1);

LAB9:    goto LAB3;

LAB5:    xsi_set_current_line(40, ng0);
    t3 = (t0 + 3464);
    t7 = (t3 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(41, ng0);
    t1 = (t0 + 2144U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 0;
    goto LAB6;

LAB8:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 2144U);
    t4 = *((char **)t1);
    t11 = *((int *)t4);
    t6 = (t11 == 1);
    if (t6 != 0)
        goto LAB11;

LAB13:    xsi_set_current_line(49, ng0);
    t1 = (t0 + 3500);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(50, ng0);
    t1 = (t0 + 2144U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t12 = (t11 + 1);
    t1 = (t0 + 2144U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t12;

LAB12:    goto LAB9;

LAB11:    xsi_set_current_line(47, ng0);
    t1 = (t0 + 3500);
    t7 = (t1 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB12;

}

static void work_a_3399415361_3212880686_p_1(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    int t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    static char *nl0[] = {&&LAB9, &&LAB10, &&LAB11, &&LAB14, &&LAB15, &&LAB16, &&LAB12, &&LAB13};

LAB0:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 568U);
    t2 = ieee_p_2592010699_sub_1258338084_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3404);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(63, ng0);
    t3 = (t0 + 684U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 1512U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (char *)((nl0) + t2);
    goto **((char **)t1);

LAB5:    xsi_set_current_line(64, ng0);
    t3 = (t0 + 3536);
    t7 = (t3 + 32U);
    t8 = *((char **)t7);
    t9 = (t8 + 40U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast(t3);

LAB6:    goto LAB3;

LAB8:    goto LAB6;

LAB9:    xsi_set_current_line(68, ng0);
    t4 = (t0 + 1880U);
    t7 = *((char **)t4);
    t5 = *((unsigned char *)t7);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB18;

LAB20:    xsi_set_current_line(73, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(74, ng0);
    t1 = (t0 + 3572);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);

LAB19:    goto LAB8;

LAB10:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(78, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(79, ng0);
    t1 = (t0 + 776U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB8;

LAB11:    xsi_set_current_line(81, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB21;

LAB23:    xsi_set_current_line(89, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(90, ng0);
    t1 = (t0 + 776U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(91, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB22:    goto LAB8;

LAB12:    xsi_set_current_line(95, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)7;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(96, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(97, ng0);
    t1 = (t0 + 868U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB8;

LAB13:    xsi_set_current_line(99, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB27;

LAB29:    xsi_set_current_line(103, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(104, ng0);
    t1 = (t0 + 868U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(105, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)7;
    xsi_driver_first_trans_fast(t1);

LAB28:    goto LAB8;

LAB14:    xsi_set_current_line(109, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(110, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 10;
    xsi_set_current_line(111, ng0);
    t1 = (t0 + 1052U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    goto LAB8;

LAB15:    xsi_set_current_line(113, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 2);
    if (t2 != 0)
        goto LAB30;

LAB32:    xsi_set_current_line(118, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(119, ng0);
    t1 = (t0 + 1052U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(120, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);

LAB31:    goto LAB8;

LAB16:    xsi_set_current_line(123, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t2 = (t12 == 1);
    if (t2 != 0)
        goto LAB33;

LAB35:    xsi_set_current_line(127, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t12 = *((int *)t3);
    t13 = (t12 - 1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(128, ng0);
    t1 = (t0 + 1144U);
    t3 = *((char **)t1);
    t1 = (t0 + 2212U);
    t4 = *((char **)t1);
    t12 = *((int *)t4);
    t13 = (t12 - 1);
    t14 = (t13 - 9);
    t15 = (t14 * -1);
    xsi_vhdl_check_range_of_index(9, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t3 + t17);
    t2 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t2;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(129, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);

LAB34:    goto LAB8;

LAB17:    xsi_set_current_line(132, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB8;

LAB18:    xsi_set_current_line(70, ng0);
    t4 = (t0 + 3536);
    t8 = (t4 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = (unsigned char)1;
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(71, ng0);
    t1 = (t0 + 3572);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB19;

LAB21:    xsi_set_current_line(82, ng0);
    t1 = (t0 + 776U);
    t4 = *((char **)t1);
    t13 = (0 - 9);
    t15 = (t13 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t4 + t17);
    t5 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t5;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(83, ng0);
    t1 = (t0 + 960U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)2);
    if (t5 != 0)
        goto LAB24;

LAB26:    xsi_set_current_line(86, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)6;
    xsi_driver_first_trans_fast(t1);

LAB25:    goto LAB22;

LAB24:    xsi_set_current_line(84, ng0);
    t1 = (t0 + 3536);
    t4 = (t1 + 32U);
    t7 = *((char **)t4);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB25;

LAB27:    xsi_set_current_line(100, ng0);
    t1 = (t0 + 868U);
    t4 = *((char **)t1);
    t13 = (0 - 9);
    t15 = (t13 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t4 + t17);
    t5 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t5;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(101, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB28;

LAB30:    xsi_set_current_line(114, ng0);
    t1 = (t0 + 1052U);
    t4 = *((char **)t1);
    t13 = (0 - 9);
    t15 = (t13 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t4 + t17);
    t5 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t5;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(115, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(116, ng0);
    t1 = (t0 + 2212U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 11;
    goto LAB31;

LAB33:    xsi_set_current_line(124, ng0);
    t1 = (t0 + 1144U);
    t4 = *((char **)t1);
    t13 = (0 - 9);
    t15 = (t13 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t1 = (t4 + t17);
    t5 = *((unsigned char *)t1);
    t7 = (t0 + 3608);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t5;
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(125, ng0);
    t1 = (t0 + 3536);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t7 = (t4 + 40U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB34;

}

static void work_a_3399415361_3212880686_p_2(char *t0)
{
    char *t1;
    unsigned char t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(141, ng0);
    t1 = (t0 + 568U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3412);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(142, ng0);
    t5 = (t0 + 1512U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)1);
    if (t8 == 1)
        goto LAB11;

LAB12:    t5 = (t0 + 1512U);
    t9 = *((char **)t5);
    t10 = *((unsigned char *)t9);
    t11 = (t10 == (unsigned char)3);
    t4 = t11;

LAB13:    if (t4 == 1)
        goto LAB8;

LAB9:    t5 = (t0 + 1512U);
    t12 = *((char **)t5);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)6);
    t3 = t14;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(145, ng0);
    t1 = (t0 + 3644);
    t5 = (t1 + 32U);
    t6 = *((char **)t5);
    t9 = (t6 + 40U);
    t12 = *((char **)t9);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);

LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(143, ng0);
    t5 = (t0 + 3644);
    t15 = (t5 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 40U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)2;
    xsi_driver_first_trans_fast(t5);
    goto LAB6;

LAB8:    t3 = (unsigned char)1;
    goto LAB10;

LAB11:    t4 = (unsigned char)1;
    goto LAB13;

}

static void work_a_3399415361_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    char *t5;
    char *t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    static char *nl0[] = {&&LAB5, &&LAB6, &&LAB9, &&LAB7, &&LAB11, &&LAB12, &&LAB8, &&LAB10};

LAB0:    t1 = (t0 + 3200U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(151, ng0);
    t2 = (t0 + 1512U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (char *)((nl0) + t4);
    goto **((char **)t2);

LAB4:    xsi_set_current_line(151, ng0);

LAB16:    t2 = (t0 + 3420);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB17;

LAB1:    return;
LAB5:    xsi_set_current_line(152, ng0);
    t5 = (t0 + 592U);
    t6 = *((char **)t5);
    t7 = *((unsigned char *)t6);
    t5 = (t0 + 3680);
    t8 = (t5 + 32U);
    t9 = *((char **)t8);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    *((unsigned char *)t11) = t7;
    xsi_driver_first_trans_fast_port(t5);
    goto LAB4;

LAB6:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1696U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB7:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1696U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB8:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1696U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB9:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB10:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB11:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB12:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t7 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t4);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t7;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB13:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 3680);
    t5 = (t2 + 32U);
    t6 = *((char **)t5);
    t8 = (t6 + 40U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB14:    t3 = (t0 + 3420);
    *((int *)t3) = 0;
    goto LAB2;

LAB15:    goto LAB14;

LAB17:    goto LAB15;

}


extern void work_a_3399415361_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3399415361_3212880686_p_0,(void *)work_a_3399415361_3212880686_p_1,(void *)work_a_3399415361_3212880686_p_2,(void *)work_a_3399415361_3212880686_p_3};
	xsi_register_didat("work_a_3399415361_3212880686", "isim/slow_test_tb_isim_beh.exe.sim/work/a_3399415361_3212880686.didat");
	xsi_register_executes(pe);
}
