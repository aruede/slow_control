The following files were generated for 'icon_inst' in directory
C:\slow_control\fw\fpga\src\user\chipscope\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * icon_inst.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * icon_inst.constraints/icon_inst.ucf
   * icon_inst.constraints/icon_inst.xdc
   * icon_inst.ngc
   * icon_inst.ucf
   * icon_inst.vhd
   * icon_inst.vho
   * icon_inst.xdc
   * icon_inst_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * icon_inst.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * icon_inst.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * icon_inst.gise
   * icon_inst.xise

Deliver Readme:
   Readme file for the IP.

   * icon_inst_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * icon_inst_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

