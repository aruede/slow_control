library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

entity ipb_slow_control_regs is
generic(addr_width : natural := 6);
port
(
	clk					: in std_logic;
	reset				: in std_logic;
	ipb_mosi_i			: in ipb_wbus;
	ipb_miso_o			: out ipb_rbus;
	------------------
	-- regs_o				: out array_32x32bit
	rd_clk : in std_logic;
	rd_en : in std_logic;
	fifo_out : out std_logic_vector( 22 downto 0 );
	fifo_empty : out std_logic;
	fifo_full : out std_logic;
	wren_cnt : out std_logic_vector(31 downto 0 );
	rden_cnt : out std_logic_vector(31 downto 0 );
	wr_fifo : out std_logic_vector(31 downto 0 );
	rd_fifo : out std_logic_vector(31 downto 0 )
);

end ipb_slow_control_regs;

architecture rtl of ipb_slow_control_regs is

	-- signal regs: array_32x32bit;		
	signal sel: integer range 0 to 31;
	signal ack: std_logic;

	attribute keep: boolean;
	attribute keep of ack: signal is true;
	attribute keep of sel: signal is true;

	signal fifo_in : std_logic_vector( 22 downto 0 );
	signal fifo_out_i : std_logic_vector( 22 downto 0 );
	signal fifo_empty_i : std_logic;
	signal fifo_full_i : std_logic;
	signal write_en : std_logic;
	
	signal control0 : std_logic_vector(35 downto 0);
	signal control1 : std_logic_vector(35 downto 0);
	
	signal wren_cnt_i : std_logic_vector(31 downto 0);
	signal rden_cnt_i : std_logic_vector(31 downto 0);

begin

icon_thing : ENTITY work.icon_inst
port map(
CONTROL0 => control0,
CONTROL1 => control1
);

ipb_ila : ENTITY work.chipscope
port map(
CONTROL => control0,
CLK => clk,
TRIG0 => fifo_in,
TRIG1(0) => write_en,
TRIG2(0) => ipb_mosi_i.ipb_write,
TRIG3 => ipb_mosi_i.ipb_wdata( 7 downto 0 )
);

fifo_out_ila : ENTITY work.chipscope
port map(
CONTROL => control1,
CLK => rd_clk,
TRIG0 => fifo_out_i,
TRIG1(0) => rd_en,
TRIG2(0) => fifo_empty_i,
TRIG3 => (others=>'0')
);

	--=============================--
	-- io mapping
	--=============================--
	-- regs_o 		<= regs;

	--=============================--
	sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width downto 0))) when addr_width>0 else 0;
	--=============================--

	--=============================--
	process(reset, clk)
	--=============================--
	-- Read data in chunks of 10 bits.
	-- this means that the inverted bits are already included in that.
	-- hub_port: H4 + H3 + H2 + H1 + nH1 + H0 + P2 + P1 + P0 + nP0
	-- hub_port_extd is for the combined slow hub double addressing
	-- addr: S6 + S5 + S4 + S3 + nS3 + S2 + S1 + S0 + RW + nRW
	-- data: D7 + D6 + D5 + D4 + nD4 + D3 + D2 + D1 + D0 + nD0
	-- extd indicated weather double addressing is used
	-- write_en detached the data stream
		variable hub_port : std_logic_vector( 9 downto 0 );
		variable hub_port_extd : std_logic_vector( 9 downto 0 );
		variable addr : std_logic_vector( 9 downto 0 );
		variable data : std_logic_vector( 9 downto 0 );
		variable extd : std_logic;
	begin
	if reset = '1' then
		-- regs <= (others=> (others=>'0'));
		ack <= '0';
		hub_port := (others=>'0');
		hub_port_extd := (others=>'0');
		addr := (others=>'0');
		data := (others=>'0');
		extd := '0';
		fifo_in <= (others=>'0');
		write_en <= '0';
	elsif rising_edge(clk) then
		-- write
		write_en <= '0';
		if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
			-- regs(sel) <= ipb_mosi_i.ipb_wdata;
			case ipb_mosi_i.ipb_addr(1 downto 0) is
				when "00" => -- Hub + port address + extended address
					hub_port := ipb_mosi_i.ipb_wdata( 9 downto 0 );
					hub_port_extd := ipb_mosi_i.ipb_wdata( 19 downto 10 );
					extd := ipb_mosi_i.ipb_wdata( 30 );
					write_en <= ipb_mosi_i.ipb_wdata( 31 );
					fifo_in <= "01" & extd & hub_port_extd & hub_port;
				when "01" => -- slave address + data
					addr := ipb_mosi_i.ipb_wdata( 9 downto 0 );
					data := ipb_mosi_i.ipb_wdata( 19 downto 10 );
					write_en <= ipb_mosi_i.ipb_wdata( 31 );
					fifo_in <= "10" & '0' & addr & data;
				when others =>
					fifo_in <= (others=>'0');
					write_en <= '0';
			end case;
		end if;
		-- read 
		ipb_miso_o.ipb_rdata <= "000000000" & fifo_in;
		-- ack
		ack <= ipb_mosi_i.ipb_strobe and not ack;
	end if;
	end process;
	
	ipb_miso_o.ipb_ack <= ack;
	ipb_miso_o.ipb_err <= '0';

	slow_control_fifo : entity work.command_fifo
	port map(
		rst => reset,
		wr_clk => clk,
		rd_clk => rd_clk,
		din => fifo_in,
		wr_en => write_en and (not fifo_full_i),
		rd_en => rd_en and (not fifo_empty_i),
		dout => fifo_out_i,
		full => fifo_full_i,
		empty => fifo_empty_i
	);

	wren_cnt_p : process( clk )
	begin
		if rising_edge( clk ) then
			if reset = '1' then
				wren_cnt_i <= (others=>'0');
			else
				if write_en = '1' then
					wren_cnt_i <= std_logic_vector(unsigned(wren_cnt_i) + 1);
					wr_fifo(22 downto 0) <= fifo_in;
				end if;
			end if;
		end if;
	end process;
	
	rden_cnt_p : process( rd_clk )
	begin
		if rising_edge( rd_clk ) then
			if reset = '1' then
				rden_cnt_i <= (others=>'0');
			else
				if rd_en = '1' then
					rden_cnt_i <= std_logic_vector(unsigned(rden_cnt_i) + 1);
					rd_fifo(22 downto 0) <= fifo_out_i;
				end if;
			end if;
		end if;
	end process;
	
	wren_cnt <= wren_cnt_i;
	rden_cnt <= rden_cnt_i;

	fifo_out <= fifo_out_i;
	fifo_full <= fifo_full_i;
	fifo_empty <= fifo_empty_i;

end rtl;