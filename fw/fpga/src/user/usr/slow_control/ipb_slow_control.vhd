library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

entity ipb_clow_control is
generic(addr_width : natural := 6);
port (
	clk : in std_logic;
	reset : in std_logic;
	ipb_mosi_i : in	ipb_wbus;
	ipb_miso_o : out ipb_rbus;
	--
	ctrl_clk : in std_logic;
	sreg_o : out array_32x32bit;
	sreg_i : in std_logic_vector( 31 downto 0 )
);
end ipb_clow_control;

architecture rtl of ipb_clow_control is

	signal regs: array_32x32bit;
	signal sregs: array_32x32bit;	

	signal sel: integer range 0 to 31;
	signal ack: std_logic;

	signal read_reg_s : std_logic_vector(31 downto 0);
	signal read_reg : std_logic_vector(31 downto 0);
	
	signal update_s : std_logic;

	attribute keep: boolean;
	attribute keep of ack: signal is true;
	attribute keep of sel: signal is true;
	attribute keep of update_s: signal is true;
	attribute keep of read_reg_s: signal is true;

	attribute ASYNC_REG : string;
  	attribute shreg_extract : string;
	attribute ASYNC_REG of update_s : signal is "TRUE";
	attribute ASYNC_REG of read_reg_s : signal is "TRUE";
	attribute shreg_extract of update_s : signal is "no";
	attribute shreg_extract of read_reg_s : signal is "no";

	signal wcyc : std_logic;

begin

	-- regs_o <= regs;
	sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width downto 0))) when addr_width>0 else 0;

	wcyc <= '1' when ipb_mosi_i.ipb_strobe = '1' and ipb_mosi_i.ipb_write = '1' else '0';

	process( reset, clk )
	begin
	if reset = '1' then
		regs <= (others=> (others=>'0'));
		ack <= '0';
	elsif rising_edge( clk ) then
		-- write
		if ipb_mosi_i.ipb_strobe = '1' and ipb_mosi_i.ipb_write = '1' then
			regs(sel) <= ipb_mosi_i.ipb_wdata;
		end if;
		-- read 
		if to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width downto 0))) < 7 then
			ipb_miso_o.ipb_rdata <= regs(sel);
		else
			ipb_miso_o.ipb_rdata <= read_reg;
		end if;
		-- ack
		ack <= ipb_mosi_i.ipb_strobe and not ack;

	end if;
	end process;
	
	ipb_miso_o.ipb_ack <= ack;
	ipb_miso_o.ipb_err <= '0';

	-- Synch

	process(ctrl_clk)
	begin
		if rising_edge(ctrl_clk) then
			-- write
			update_s <= wcyc or reset; -- Synchroniser reg
			update <= update_s;
			if update_s = '1' then
				sregs <= regs;
			end if;
			-- read
			read_reg_s <= sreg_i;
			read_reg <= read_reg_s;
		end if;
	end process;
	
	sreg_o <= sregs;

end rtl;
