library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity slow_test is
port
(
	clk_90deg : in std_logic;
	reset : in std_logic;
	hub_port : in std_logic_vector( 9 downto 0 );
	hub_port_extd  : in std_logic_vector( 9 downto 0 );
	extd : in std_logic;
	addr : in std_logic_vector( 9 downto 0 );
	data : in std_logic_vector( 9 downto 0 );
	disp : in std_logic;
	ready : out std_logic;
	ser_data : out std_logic
);
end slow_test;

architecture Behavioral of slow_test is

	type state_t is ( idle, start_hub_port, write_hub_port, start_addr, write_addr, write_data, start_hub_port_extd, write_hub_port_extd );
	signal state : state_t;
	
	signal outputs : std_logic;
	signal start_flg : std_logic;
	
	signal high_flag : std_logic;
	signal disp_slfclr : std_logic;
	
begin

--	-- self clearing for dispatch signal
--	selfclear_p : process(clk_90deg)
--	begin
--		if rising_edge(clk_90deg) then
--			if disp = '1' then
--				high_flag <= '1';
--			else
--				high_flag <= '0';
--			end if;
--			if high_flag = '1' then
--				disp_slfclr <= '0';
--			else
--				disp_slfclr <= disp;
--			end if;
--		end if;
--	end process;


	state_machine_p : process( clk_90deg )
		variable cnt : integer := 10;
	begin
		if falling_edge( clk_90deg ) then
			if reset = '1' then
				state <= idle;
			else
				case state is
					when idle => 
--						if disp_slfclr = '1' then
						if disp = '1' then
							state <= start_hub_port;
							ready <= '0';
						else
							state <= idle;
							ready <= '1';
						end if;
					when start_hub_port => 
						state <= write_hub_port;
						cnt := 10;
						outputs <= hub_port(cnt-1);
					when write_hub_port =>
						if cnt = 1 then
							outputs <= hub_port(0);
							if extd = '0' then
								state <= start_addr;
							else
								state <= start_hub_port_extd;
							end if;
						else
							cnt := cnt - 1;
							outputs <= hub_port(cnt-1);
							state <= write_hub_port;
						end if;
					-------------------
					when start_hub_port_extd => 
						state <= write_hub_port_extd;
						cnt := 10;
						outputs <= hub_port_extd(cnt-1);
					when write_hub_port_extd =>
						if cnt = 1 then
							outputs <= hub_port_extd(0);
							state <= start_addr;
						else
							cnt := cnt - 1;
							outputs <= hub_port_extd(cnt-1);
							state <= write_hub_port_extd;
						end if;
					-------------------
					when start_addr =>
						state <= write_addr;
						cnt := 10;
						outputs <= addr(cnt-1);
					when write_addr =>
						if cnt = 2 then
							outputs <= addr(0);
							state <= write_data;
							cnt := 11;
						else
							cnt := cnt - 1;
							outputs <= addr(cnt-1);
							state <= write_addr;
						end if;
					when write_data =>
						if cnt = 1 then
							outputs <= data(0);
							state <= idle;
						else
							cnt := cnt - 1;
							outputs <= data(cnt-1);
							state <= write_data;
						end if;
					when others =>
						state <= idle;
				end case;
			end if;
		end if;
	end process;	


	startstop_p : process( clk_90deg )
	begin
		if rising_edge( clk_90deg ) then
			if state = start_hub_port or state = start_addr or state = start_hub_port_extd then
				start_flg <= '0';
			else
				start_flg <= '1';
			end if;
		end if;
	end process;


	with state select ser_data <= 
		clk_90deg when idle,
		start_flg when start_hub_port,
		start_flg when start_addr,
		start_flg when start_hub_port_extd,
		not outputs when write_hub_port, --change
		not outputs when write_hub_port_extd, --change
		not outputs when write_addr, --change
		not outputs when write_data, --change
		clk_90deg when others;

end Behavioral;

