library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

entity slow_controller is
generic(addr_width : natural := 2);
port
(
	clk : in std_logic;
	clk_90deg : in std_logic;
	reset : std_logic;
	fifo_data : in std_logic_vector( 22 downto 0 );
	fifo_empty : in std_logic;
	rd_en : out std_logic;
	ser_data : out std_logic
);

end slow_controller;

architecture behavioral of slow_controller is

	type state_t is ( idle, start, stopp, read_fifo, send_ser_data );
	signal state : state_t;

	signal start_flg : std_logic;
	signal stop_flg : std_logic;

	signal fifo_data_store : std_logic_vector( 22 downto 0 );
	signal data : std_logic;

begin

	readout_state_machine : process( clk_90deg )
		variable cnt : integer := 10;
	begin
		if falling_edge( clk_90deg ) then
			if reset = '1' then
				state <= idle;
				rd_en <= '0';
			else
				rd_en <= '0';
				case state is
					when idle => 
						if fifo_empty = '0' then
							state <= read_fifo;
							fifo_data_store <= fifo_data;
							rd_en <= '1';
						else
							state <= idle;
						end if;
					when read_fifo =>
						if fifo_data_store( 22 downto 21 ) = "01" then
							state <= start;
						elsif fifo_data_store( 22 downto 21 ) = "10" then
							state <= start;
						else
							state <= idle;
						end if;
					when start =>
						state <= send_ser_data;
						if fifo_data_store( 22 downto 21 ) = "01" then
							cnt := 9;
						elsif fifo_data_store( 22 downto 21 ) = "10" then
							cnt := 19;
						end if;
						data <= fifo_data_store(cnt);
					when send_ser_data =>
						if cnt = 1 then
							data <= fifo_data_store(0);
							if fifo_data_store( 22 downto 21 ) = "01" then
								fifo_data_store <= fifo_data;
								rd_en <= '1';
								state <= start;
							elsif fifo_data_store( 22 downto 21 ) = "10" then
								state <= stopp;
							end if;
						else 
							state <= send_ser_data;
							cnt := cnt - 1;
							data <= fifo_data_store(cnt);
						end if;
					when stopp =>
						state <= idle;
					when others =>
						state <= idle;
				end case;
			end if;
		end if;
	end process;

	startstop_p : process( clk_90deg )
	begin
		if rising_edge( clk_90deg ) then
			if state = start then
				start_flg <= '0';
			else
				start_flg <= '1';
			end if;
			if state = stopp then
				stop_flg <= '1';
			else
				stop_flg <= '0';
			end if;
		end if;
	end process;
	
	with state select ser_data <= 
		clk_90deg when idle,
		clk_90deg when read_fifo,
		start_flg when start,
		data when send_ser_data,
		-- stop_flg when stopp,
		data when stopp,
		clk_90deg when others;

end behavioral;