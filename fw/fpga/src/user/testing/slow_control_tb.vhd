library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

ENTITY slow_control_tb IS
END slow_control_tb;

ARCHITECTURE behavior OF slow_control_tb IS 


	signal reset : std_logic := '1';
	signal clk_40 : std_logic;
	signal clk_40_90deg : std_logic;
	signal ipb_clk : std_logic := '1';
	signal clk_125 : std_logic := '1';

	signal fifo_data : std_logic_vector( 22 downto 0 );
	signal fifo_empty : std_logic;
	signal fifo_full : std_logic;

	signal rd_en : std_logic;
	signal ser_data : std_logic;

	signal ipb_mosi_i : ipb_wbus;
	signal ipb_miso_o : ipb_rbus;
	
	signal locked : std_logic;
	
	signal wren_cnt : std_logic_vector( 31 downto 0 );
	signal rden_cnt : std_logic_vector( 31 downto 0 );
	signal wr_fifo : std_logic_vector( 31 downto 0 );
	signal rd_fifo : std_logic_vector( 31 downto 0 );
	
begin

	rst_p : process begin wait for 100 ns; reset <= not reset; wait; end process;
	clk125_p : process begin  wait for 62.5 ns; clk_125 <= not clk_125; end process;
	ipbclk_p : process begin  wait for 15 ns; ipb_clk <= not ipb_clk; end process;

	clk_generator : entity work.clk_gen
	port map(
		usr_clk => clk_125,
		rst => reset,
		locked => locked,
		clk_40 => clk_40,
		clk_40_90deg => clk_40_90deg
	);

	ipbus : entity work.ipb_slow_control_regs
	port map(
		clk => ipb_clk,
		reset => reset,
		ipb_mosi_i => ipb_mosi_i,
		ipb_miso_o => ipb_miso_o,
		rd_clk => clk_40,
		rd_en => rd_en,
		fifo_out => fifo_data,
		fifo_empty => fifo_empty,
		fifo_full => fifo_full,
		wren_cnt => wren_cnt,
		rden_cnt => rden_cnt,
		wr_fifo => wr_fifo,
		rd_fifo => rd_fifo
	);

	controller : entity work.slow_controller
	port map(
		clk => clk_40,
		clk_90deg => clk_40_90deg,
		reset => reset,
		fifo_data => fifo_data,
		fifo_empty => fifo_empty,
		rd_en => rd_en,
		ser_data => ser_data
	);

	ipb_p : process
	begin
		ipb_mosi_i.ipb_addr( 31 downto 2) <= (others=>'0');
		ipb_mosi_i.ipb_wdata( 29 downto 20 ) <= (others=>'0');
		ipb_mosi_i.ipb_strobe <= '0';
		ipb_mosi_i.ipb_write <= '0';
		wait until locked = '1';
		wait until rising_edge( ipb_clk );
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_write <= '1';
		ipb_mosi_i.ipb_addr(1 downto 0) <= "00";
		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "111101" & "0001";
		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= (others=>'0');
		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
		 wait until rising_edge( ipb_clk );
		 ipb_mosi_i.ipb_strobe <= '0';
		 ipb_mosi_i.ipb_write <= '0';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '1';
--		ipb_mosi_i.ipb_write <= '1';
--		ipb_mosi_i.ipb_addr(1 downto 0) <= "01";
--		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "000010" & "1110";
--		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= "110010" & "1101";
--		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
--		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '1';
--		ipb_mosi_i.ipb_write <= '1';
--		ipb_mosi_i.ipb_addr(1 downto 0) <= "01";
--		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "1010101010";
--		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= "0101010101";
--		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
--		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '0';
--		ipb_mosi_i.ipb_write <= '0';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '1';
--		ipb_mosi_i.ipb_write <= '1';
--		ipb_mosi_i.ipb_addr(1 downto 0) <= "00";
--		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "111101" & "0001";
--		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= (others=>'0');
--		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
--		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '1';
--		ipb_mosi_i.ipb_write <= '1';
--		ipb_mosi_i.ipb_addr(1 downto 0) <= "01";
--		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "000010" & "1110";
--		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= "110010" & "1101";
--		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
--		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '1';
--		ipb_mosi_i.ipb_write <= '1';
--		ipb_mosi_i.ipb_addr(1 downto 0) <= "01";
--		ipb_mosi_i.ipb_wdata( 9 downto 0 ) <= "1010101010";
--		ipb_mosi_i.ipb_wdata( 19 downto 10 ) <= "0101010101";
--		ipb_mosi_i.ipb_wdata( 30 ) <= '0';
--		ipb_mosi_i.ipb_wdata( 31 ) <= '1';
--		wait until rising_edge( ipb_clk );
--		ipb_mosi_i.ipb_strobe <= '0';
--		ipb_mosi_i.ipb_write <= '0';
		wait;
	end process;

end;
