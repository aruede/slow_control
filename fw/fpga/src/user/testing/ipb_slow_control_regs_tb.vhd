library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

ENTITY ipb_slow_control_regs_tb IS
END ipb_slow_control_regs_tb;

ARCHITECTURE behavior OF ipb_slow_control_regs_tb IS 


	signal clk : std_logic := '1';
	signal reset : std_logic;
	signal ipb_mosi_i : ipb_wbus;
	signal ipb_miso_o : ipb_rbus;
	signal ctrl_clk : std_logic := '1';
	signal sreg_o : array_32x32bit;
	signal sreg_i : std_logic_vector;
	
BEGIN

	DUT : entity work.ipb_slow_control
	port map(
		clk => clk,
		reset => reset,
		ipb_mosi_i => ipb_mosi_i,
		ipb_miso_o => ipb_miso_o,
		ctrl_clk => ctrl_clk,
		sreg_o => sreg_o,
		sreg_i => sreg_i
	);
	
	rst_p : process
	begin
		reset <= '1';
		wait for 100 ns;
		reset <= '0';
		wait;
	end process;
	
	clk_p : process	begin clk <= not clk; wait for 33 ns; end process;
	ctrl_clk_p : process begin ctrl_clk <= not ctrl_clk; wait for 25 ns; end process;

	stat_p : process
	begin
		sreg_i <= (others=> '0');
		sreg_i(7 downto 0) <= "10101111"; 
	end process;

	ipb_p : process
	begin
		wait for 200 ns;
		wait until rising_edge( clk );
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned( 7, ipb_mosi_i.ipb_addr'length ) );
		wait until rising_edge( clk );
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned( 3, ipb_mosi_i.ipb_addr'length ) );
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned( 9, ipb_mosi_i.ipb_addr'length ) );
		wait;
	end process;

END;
