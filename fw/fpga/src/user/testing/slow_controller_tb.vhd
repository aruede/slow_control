library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

ENTITY slow_controller_tb IS
END slow_controller_tb;

ARCHITECTURE behavior OF slow_controller_tb IS 


	signal clk : std_logic;
	signal clk_90deg : std_logic;
	signal reset : std_logic;
	signal fifo_data : std_logic_vector( 22 downto 0 );
	signal fifo_empty : std_logic;
	signal rd_en : std_logic;
	signal ser_data : std_logic;
	
	signal clk_125 : std_logic;
	signal locked : std_logic;
	constant clk_per : time := 30 ns;
	
BEGIN

	DUT : entity work.slow_controller
	port map(
		clk => clk,
		clk_90deg => clk_90deg,
		reset => reset,
		fifo_data => fifo_data,
		fifo_empty => fifo_empty,
		rd_en => rd_en,
		ser_data => ser_data
	);
	
	rst_p : process
	begin
		reset <= '1';
		wait for 100 ns;
		reset <= '0';
		wait;
	end process;
	
	clk_p : process
	begin
		clk_125 <= '1';
		wait for 60 ns;
		clk_125 <= '0';
		wait for 60 ns;
	end process;

	clk_generator : entity work.clk_gen
	port map(
		usr_clk => clk_125,
		rst => reset,
		locked => locked,
		clk_40 => clk,
		clk_40_90deg => clk_90deg
	);

	sequence_p : process
	begin
		fifo_empty <= '1';
		wait until locked = '1';
		wait for 500 ns;
		wait until rising_edge(clk);
		fifo_empty <= '0';
		wait until rising_edge(clk);
		fifo_empty <= '1';
		wait;
	end process;
	

END;
