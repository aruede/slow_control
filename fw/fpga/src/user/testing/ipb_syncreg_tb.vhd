library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

ENTITY ipb_syncreg_tb IS
END ipb_syncreg_tb;

ARCHITECTURE behavior OF ipb_syncreg_tb IS 

	signal clk : std_logic := '1';
	signal slv_clk : std_logic := '1';
	signal reset : std_logic := '1';

	signal ipb_mosi_i : ipb_wbus;
	signal ipb_miso_o : ipb_rbus;
	

	constant stat_addr_width : natural := 1;
	constant ctrl_addr_width : natural := 3;
	signal d : std_logic_vector(2 ** stat_addr_width * 32 - 1 downto 0);
	signal q : std_logic_vector(2 ** ctrl_addr_width * 32 - 1 downto 0);
	signal stb : std_logic_vector(2 ** ctrl_addr_width - 1 downto 0);
	
begin

	rst_p : process begin wait for 100 ns; reset <= not reset; wait; end process;
	clk_p : process begin  wait for 33 ns; clk <= not clk; end process;
	slv_clk_p : process begin  wait for 25 ns; slv_clk <= not slv_clk; end process;

	DUT : entity work.ipbus_syncreg
	generic map(
		stat_addr_width => stat_addr_width,
		ctrl_addr_width => ctrl_addr_width
	)
	port map(
		clk => clk,
		rst => reset,
		ipb_in => ipb_mosi_i,
		ipb_out => ipb_miso_o,
		slv_clk => slv_clk,
		d => d,
		q => q,
		stb => stb
	);

	stat_p : process
	begin
		d <= (others => '0');
		wait for 400 ns;
		d( 7 downto 0) <= "10101111";
		wait;
	end process;


	ipb_p : process
	begin
		wait for 200 ns;
		ipb_mosi_i.ipb_write <= '0';
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_write <= '0';
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(0, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(1, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(2, ipb_mosi_i.ipb_addr'length) );
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '0';
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(3, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(4, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(5, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(6, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(7, ipb_mosi_i.ipb_addr'length) );
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '0';
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(8, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(9, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(10, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(11, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(12, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(13, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(14, ipb_mosi_i.ipb_addr'length) );
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '0';
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(15, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(16, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(17, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(18, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(19, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(20, ipb_mosi_i.ipb_addr'length) );
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_strobe <= '0';
		--
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_write <= '1';
		ipb_mosi_i.ipb_strobe <= '1';
		ipb_mosi_i.ipb_addr <= std_logic_vector( to_unsigned(1, ipb_mosi_i.ipb_addr'length) );
		ipb_mosi_i.ipb_wdata <= std_logic_vector( to_unsigned(4, ipb_mosi_i.ipb_addr'length) );
		wait until rising_edge(clk);
		ipb_mosi_i.ipb_write <= '0';
		ipb_mosi_i.ipb_strobe <= '0';
		wait;
	end process;


end;
