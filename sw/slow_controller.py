# Developer: Alexander Ruede (CERN/KIT-IPE)
# email: aruede@cern.ch

# BCM1F Slow Control

import sys
from pyControl import *
from time import sleep

data = int(sys.argv[1])

# Create object, specifying the IP address
# and connecting to the hardware IP endpoint
sc = pyControl( "128.141.58.146" )
sleep(0.2)

sc.reset()

sc.fmc_i2c_init()
sc.fmc_i2c_config()


port=1
rw = 0
address = 0x61
data = 0x21

for data in range(64):
    sc.write_command( port=port, address=address+0, data=data, rw=rw)
    sc.write_command( port=port, address=address+1, data=data, rw=rw)
    sc.write_command( port=port, address=address+2, data=data, rw=rw)
    sc.write_command( port=port, address=address+3, data=0, rw=rw)
    sleep(0.1)
